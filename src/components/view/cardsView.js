

import CardPreGame from '../common/card/cardPreGame';
import CardLiveGame from '../common/card/cardLiveGame';
import CardPostGame from '../common/card/cardPostGame';


//import styles from '../styles/Home.module.css'
//import styles from '../../styles/Home.module.css'
import Box from "@mui/material/Box";
import Tab from '@mui/material/Tab';

import Typography from "@mui/material/Typography";
import styles from "./cardView.module.css";

import Grid from "@mui/material/Grid";


import Stack from '@mui/material/Stack';
import TextField from '@mui/material/TextField';
import AdapterDateFns from '@mui/lab/AdapterDateFns';

import LocalizationProvider from '@mui/lab/LocalizationProvider';
//import TimePicker from '@mui/lab/TimePicker';
//import DateTimePicker from '@mui/lab/DateTimePicker';
import DesktopDatePicker from '@mui/lab/DesktopDatePicker';

import React, { useState, useEffect } from "react";
import axios from "axios";

const urlGameLive = "http://3.235.98.253:8080/GameLive/2016-03-05";
const urilGamePost = "http://3.235.98.253:8080/GamePost/2015-11-14";

const CardView = () => {

  const [dataPreGame, setDataPreGame] = useState([]);
  const [dataLiveGame, setDataLiveGame] = useState([]);
  const [dataPostGame, setDataPostGame] = useState([]);


  const [reload, setReload] = useState(false);


  /*date output format,Reference to get date data*/
  //const [value, setValue] = React.useState(new Date('2015-11-17T21:11:54'));

  /*current current date automatically */
  const [currentDate, setCurrentDate] = useState(new Date());
  const [currentDateMiddle, setCurrentDateMiddle] = useState(new Date());
  const [previousDate, setPreviousDate] = useState(new Date());
  const [nextDate, setNextDate] = useState(new Date());

  const ClickPreviousDate = () => {
   
    setCurrentDate(previousDate);
    if (reload == false) {
      setReload(true);
      return;
    }

    if (reload == true) {
      setReload(false);
      return;
    }
  }

  const ClickNextDate = () => {
   
    setCurrentDate(nextDate);
    if (reload == false) {
      setReload(true);
      return;
    }

    if (reload == true) {
      setReload(false);
      return;
    }
  }
  const handleChange = (fecha) => {
    
    //   const fecha = new Date(fecha);
    //   console.log("valor de la fecha: ",fecha.getDate());
    // console.log("valor de newValue: ",newValue.getDate());


    const yesterdayDate = () => {
      // let hoy = new Date(fecha);
      let DIA_EN_MILISEGUNDOS = 24 * 60 * 60 * 1000;
      let ayer = new Date(fecha.getTime() - DIA_EN_MILISEGUNDOS);
      return ayer;
    }

    const tomorrowDate = () => {
      // let hoy = new Date(fecha);
      let DIA_EN_MILISEGUNDOS = 24 * 60 * 60 * 1000;
      let manana = new Date(fecha.getTime() + DIA_EN_MILISEGUNDOS);
      return manana;
    };

    console.log(yesterdayDate())

    /*Actulizadon la fechas */
    setCurrentDate(fecha);
    setCurrentDateMiddle(fecha);

    setPreviousDate(yesterdayDate());
    setNextDate(tomorrowDate());

    //console.log("valor de previus Date :  ", previousDate)


    if (reload == false) {
      setReload(true);
      return;
    }

    if (reload == true) {
      setReload(false);
      return;
    }


  };

  useEffect(() => {
    console.log("simulating reload")
    loadDataLiveGame();
    loadDataPreGame();
    loadDataPostGame();


  }, [reload])

  useEffect(() => {
    console.log("only the first time")
    loadDataPreGame();
    loadDataPostGame();

    /**Fechas siguiente y anterior */
    testPreviousDate();
  }, [])

  useEffect(() => {
    const interval = setInterval(() => {
      console.log("calling api every minute")
      loadDataLiveGame();

    }, 60000);
    return () => clearInterval(interval);

  }, [])

  const getDay = (tempDateDay) => {
    let res = "0";
    if (tempDateDay > 0 && tempDateDay < 10) {
      res = res + tempDateDay;
      return res;
    } else {
      return tempDateDay;
    }
  }

  const getMonth = (tempDateMonth) => {
    let res = "0";
    if (tempDateMonth > 0 && tempDateMonth < 10) {
      let temp = tempDateMonth + 1;
      res = res + temp;
      return res;
    } else {
      return tempDateMonth + 1;
    }
  }
  /*
    const test = () => {
      const res = `Server/${currentDate.getFullYear()}-${getDay(currentDate.getDate())}-${getMonth(currentDate.getMonth())}`;
      return res;
    }*/

  /*
  var date = new Date();

date ; //# => Fri Apr 01 2011 11:14:50 GMT+0200 (CEST)

date.setDate(date.getDate() - 1);

date ; //# => Thu Mar 31 2011 11:14:50 GMT+0200 (CEST)*/

  const testPreviousDate = () => {



    setPreviousDate((previous) => {
      previous.setDate(currentDate.getDate() - 1);
      return previous;
    })

    setNextDate((previous) => {
      previous.setDate(currentDate.getDate() + 1);
      return previous;
    })


  }

  /*formateado de meses fechas dinamicas*/



  const loadDataPreGame = async () => {
    {/*console.log("anio" ,currentDate.getFullYear());
    console.log("mes ",getMonth(currentDate.getMonth()) );
  console.log("dia ",getDay(currentDate.getDate()));*/}

    const result = await axios.get(`http://3.235.98.253:8080/GamePre/${currentDate.getFullYear()}-${getMonth(currentDate.getMonth())}-${getDay(currentDate.getDate())}`);
    //Reference result = await axios.get(`http://34.227.80.167:8080/GamePre/2021-05-13`);
    setDataPreGame(result.data);   // setUser(result.data.reverse());
   // console.log(result.data);
  }


  const loadDataLiveGame = async () => {

    const result = await axios.get(`http://3.235.98.253:8080/GameLive/${currentDate.getFullYear()}-${getMonth(currentDate.getMonth())}-${getDay(currentDate.getDate())}`);

    setDataLiveGame(result.data);
  //  console.log(result.data);
  }

  const loadDataPostGame = async () => {

    const result = await axios.get(`http://3.235.98.253:8080/GamePost/${currentDate.getFullYear()}-${getMonth(currentDate.getMonth())}-${getDay(currentDate.getDate())}`);

    setDataPostGame(result.data);
   // console.log(result.data);
  }




  return (
    <div>
      <Typography component="div" sx={{
        display: 'flex', justifyContent: 'space-around',
        alignItems: 'center', margin: '1rem'
      }}>
        <Typography className={styles.dynamicDate} onClick={ClickPreviousDate}>
          {previousDate.getFullYear() + "-" + previousDate.getDate() + "-" + getMonth(previousDate.getMonth())}
        </Typography>
        <LocalizationProvider dateAdapter={AdapterDateFns} >
          <Stack spacing={3}>
            <DesktopDatePicker

              inputFormat="MM/dd/yyyy"
              value={currentDateMiddle}
              onChange={handleChange}
              renderInput={(params) => <TextField {...params} />}
            />


          </Stack>

        </LocalizationProvider>
        <Typography className={styles.dynamicDate} onClick={ClickNextDate} >
          {nextDate.getFullYear() + "-" + nextDate.getDate() + "-" + getMonth(nextDate.getMonth())}
        </Typography>


      </Typography>

      <div className={styles.container}>




        <Box sx={{ flexGrow: 1 }}>
          <Grid
            container
            spacing={{ xs: 1, sm: 1, md: 1 }}
            columns={{ xs: 12, sm: 12, md: 12 }}
          >
            {!dataLiveGame ? 'Load...' :
              dataLiveGame.map((element, index) => {
                return (
                  <Grid item xs={12} sm={12} md={6} key={index} >

                    <CardLiveGame

                      awayTeam={element.awayTeam}
                      homeTeam={element.homeTeam}

                      awayTeamLogoUrl={element.awayTeamLogoUrl}
                      homeTeamLogoUrl={element.homeTeamLogoUrl}

                      conference={element.conference}

                      awayTeamID={element.awayTeamID}
                      homeTeamID={element.homeTeamID}

                      period={element.period}

                      timeRemainingMinutes={element.timeRemainingMinutes}
                      timeRemainingSeconds={element.timeRemainingSeconds}

                      awayTeamScore={element.awayTeamScore}
                      homeTeamScore={element.homeTeamScore}

                      periodsAwayScore={element.periodsAwayScore}
                      periodsHomeScore={element.periodsHomeScore}
                      periodsAwayScoreX={element.periodsAwayScoreX}
                      periodsHomeScoreX={element.periodsHomeScoreX}

                      pointSpread={element.pointSpread}
                      overUnder={element.overUnder}

                      liveOddsOverUnder={element.liveOddsOverUnder}
                      liveOddsAwayPointSpread={element.liveOddsAwayPointSpread}

                    />
                  </Grid>
                )
              })}

            {!dataPreGame ? 'Load...' :
              dataPreGame.map((element, index) => {
                return (
                  <Grid item xs={12} sm={12} md={6} key={index} >

                    <CardPreGame awayTeam={element.awayTeam}
                      awayPointSpreadPayout={element.awayPointSpreadPayout}
                      awayTeamLogoUrl={element.awayTeamLogoUrl}

                      awayTeamID={element.awayTeamID}
                      homeTeamID={element.homeTeamID}

                      homeTeamLogoUrl={element.homeTeamLogoUrl}
                      dateTime={element.dateTime}

                      pregameOddsOverUnderX={element.pregameOddsOverUnderX}
                      pregameOddsOverUnder={element.pregameOddsOverUnder}

                      homeTeam={element.homeTeam}

                      pregameOddsAwayPointSpreadX={element.pregameOddsAwayPointSpreadX}
                      pregameOddsAwayPointSpread={element.pregameOddsAwayPointSpread}

                      awayBettingMarketSplitsBetPercentage={element.awayBettingMarketSplitsBetPercentage}

                      pregameOddsHomePointSpread={element.pregameOddsHomePointSpread}

                      homeBettingMarketSplitsBetPercentage={element.homeBettingMarketSplitsBetPercentage}

                      awayWins={element.awayWins}
                      awayLosses={element.awayLosses}

                      awayConferenceWins={element.awayConferenceWins}
                      awayConferenceLosses={element.awayConferenceLosses}

                      awayTeamGameTrendsWins={element.awayTeamGameTrendsWins}
                      awayTeamGameTrendsLosses={element.awayTeamGameTrendsLosses}

                      homeTeamGameTrendsWins={element.homeTeamGameTrendsWins}
                      homeTeamGameTrendsLosses={element.homeTeamGameTrendsLosses}

                      homeConferenceWins={element.homeConferenceWins}
                      homeConferenceLosses={element.homeConferenceLosses}

                      homeTeamGameTrendsWins={element.homeTeamGameTrendsWins}
                      homeTeamGameTrendsLosses={element.homeTeamGameTrendsLosses}
                    />
                  </Grid>
                )
              })}

            {!dataPostGame ? 'Load...' :
              dataPostGame.map((element, index) => {
                return (
                  <Grid item xs={12} sm={12} md={6} key={index} >

                    <CardPostGame

                      awayTeam={element.awayTeam}
                      homeTeam={element.homeTeam}

                      awayTeamLogoUrl={element.awayTeamLogoUrl}
                      homeTeamLogoUrl={element.homeTeamLogoUrl}

                      conference={element.conference}

                      awayTeamID={element.awayTeamID}
                      homeTeamID={element.homeTeamID}

                      period={element.period}


                      awayTeamScore={element.awayTeamScore}
                      homeTeamScore={element.homeTeamScore}

                      periodsAwayScore={element.periodsAwayScore}
                      periodsHomeScore={element.periodsHomeScore}
                      periodsAwayScoreX={element.periodsAwayScoreX}
                      periodsHomeScoreX={element.periodsHomeScoreX}

                      pointSpread={element.pointSpread}
                      overUnder={element.overUnder}

                      liveOddsOverUnder={element.liveOddsOverUnder}
                      liveOddsAwayPointSpread={element.liveOddsAwayPointSpread}

                    />
                  </Grid>
                )
              })}


          </Grid>
        </Box>

      </div>
    </div>
  )
}

export default CardView;