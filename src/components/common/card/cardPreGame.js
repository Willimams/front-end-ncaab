import React, { useState, useEffect } from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { CardActionArea } from "@mui/material";
import styles from "./demo.module.css";

import { pink } from '@mui/material/colors';
import Checkbox from '@mui/material/Checkbox';

const label = { inputProps: { 'aria-label': 'Checkbox demo' } };


const ActionAreaCard = (data) => {


   // console.log(data)

    return (
        <Card sx={{ maxWidth: '100%' }}>

            <CardActionArea>

                <Typography className={styles.cajaNegra} component="div">
                    <Checkbox className={styles.checked} checked={true} color="default" size="small" />
                    <Typography className={styles.title} sx={{ fontSize: '14px' }} gutterBottom variant="h1" component="div">
                        {data.awayTeam} at {data.homeTeam}
                    </Typography>
                </Typography>
                <CardContent sx={{ padding: '0', margin: '0', display: 'flex', justifyContent: 'space-between', alignItems: 'center' }} >

                    {/*Container Left*/}
                    <CardContent className={styles.containerleft} sx={{
                        width: '25%',
                        display: 'flex', flexDirection: 'column', alignItems: 'flex-start'
                    }} >
                        <Typography className={styles.subboxleft}
                            component="div">

                            <Typography sx={{ marginRight: '0.4rem', fontWeight: '800' }} gutterBottom variant="h5" component="div">
                                {data.awayTeam}
                            </Typography>
                            <Typography sx={{ fontSize: '15px', marginRight: '0.4rem', fontWeight: '400' }} gutterBottom variant="h5" component="div">
                                {data.awayTeamID}
                            </Typography>
                        </Typography>

                        <CardMedia
                            component="img"
                     

                            image={data.awayTeamLogoUrl}
                            alt="imagen"

                            className={styles.img}
                        />
                        <Typography className={styles.subboxleft} component="div">

                            <Typography sx={{ marginRight: '0.4rem', fontWeight: '800' }} gutterBottom variant="h5" component="div">
                                {data.awayBettingMarketSplitsBetPercentage} %
                            </Typography>
                            <Typography sx={{ fontSize: '15px', marginRight: '0.4rem', fontWeight: '400' }} gutterBottom variant="h5" component="div">
                                {data.pregameOddsHomePointSpread}
                            </Typography>
                        </Typography>

                    </CardContent>

                    {/*Contianer middle */}
                    <CardContent sx={{
                        width: '50%',

                        margin: '0', padding: '0', display: 'flex', flexDirection: 'column', alignItems: 'center'
                    }}>
                        <CardContent sx={{ display: 'flex', justifyContent: 'center', padding: '0' }}>

                            <Typography className={styles.iconsimulation} sx={{
                                background: '#093D27', borderRadius: '30px',
                                padding: '0.3rem', color: 'white', fontSize: '12px', height: '15px',
                                marginTop: '0.5rem', marginLeft: '1rem', marginRight: '1rem'
                            }}
                                gutterBottom variant="h5" component="div">
                                AT
                            </Typography>


                        </CardContent>
                        <Typography className={styles.hour} sx={{ fontSize: '15px', fontWeight: '600', color: 'black' }} gutterBottom variant="h5" component="div">
                            {data.dateTime}
                        </Typography>
                        <Typography sx={{ display: 'flex', justifyContent: 'center' }} component="div">
                            <CardContent sx={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-end' }}>
                                <Typography sx={{ display: 'flex' }} component="div">
                                    <Typography sx={{ fontSize: '13px', fontWeight: '800' }} gutterBottom variant="h5" component="div">
                                        OPEN:
                                    </Typography>
                                    <Typography sx={{ fontSize: '13px', fontWeight: '400' }} gutterBottom variant="h5" component="div">
                                        O/U: {data.pregameOddsOverUnderX} {data.homeTeam} +{data.pregameOddsAwayPointSpreadX}
                                    </Typography>
                                </Typography>
                                <Typography sx={{ display: 'flex' }} component="div">
                                    <Typography sx={{ fontSize: '13px', fontWeight: '800' }} gutterBottom variant="h5" component="div">
                                        LIVE:
                                    </Typography>
                                    <Typography sx={{ fontSize: '13px', fontWeight: '400' }} gutterBottom variant="h5" component="div">
                                        O/U: {data.pregameOddsOverUnder} {data.homeTeam} +{data.pregameOddsAwayPointSpread}
                                    </Typography>
                                </Typography>


                            </CardContent>

                        </Typography>
                    </CardContent>

                    {/*Container Right*/}
                    <CardContent className={styles.containerleft} sx={{
                         width: '25%',
                        display: 'flex', flexDirection: 'column', alignItems: 'flex-end'
                    }}>
                        <Typography className={styles.subboxright}

                            component="div">
                            <Typography sx={{ marginRight: '0.5rem', fontSize: '15px', fontWeight: '400' }} gutterBottom variant="h5" component="div">
                                {data.homeTeamID}
                            </Typography>
                            <Typography sx={{ fontWeight: '800' }} gutterBottom variant="h5" component="div">
                                {data.homeTeam}
                            </Typography>

                        </Typography>

                        <CardMedia
                            component="img"
                            /*height="140"*/

                            image={data.homeTeamLogoUrl}
                            alt="imagen"
                            className={styles.img}
                        />
                        <Typography className={styles.subboxright}

                            component="div">
                            <Typography sx={{ fontSize: '15px', fontWeight: '400', marginRight: '0.5rem' }} gutterBottom variant="h5" component="div">
                                +{data.pregameOddsAwayPointSpread}
                            </Typography>
                            <Typography sx={{ fontWeight: '800' }} gutterBottom variant="h5" component="div">
                                {data.homeBettingMarketSplitsBetPercentage} %
                            </Typography>
                        </Typography>
                    </CardContent>


                </CardContent>
                <Typography sx={{
                    borderTop: '2px solid #C8C8C8', borderBottom: '2px solid #C8C8C8'
                    , paddingTop: '0.4rem', paddingBottom: '0', display: 'flex', justifyContent: 'space-between', alignItems: 'center'
                }} component="div">
                    <Typography sx={{
                        backgroundColor: '#202758 ', fontSize: '0.9rem', color: 'white',
                        borderRadius: '10%', padding: '0.3rem'
                    }} gutterBottom variant="h5" component="div">
                        PICK
                    </Typography>
                    <Typography className={styles.subtitulos}  gutterBottom variant="h5" component="div">
                        Preview  Trends Consensus Line Movement
                    </Typography>
                    <Typography sx={{
                        backgroundColor: '#202758 ', fontSize: '0.9rem', color: 'white',
                        borderRadius: '10%', padding: '0.3rem'
                    }} gutterBottom variant="h5" component="div">
                        MATCHUP
                    </Typography>
                </Typography>

                <Typography className={styles.tablatwo} component="div">
                    <Typography className={styles.subtablatwo} sx={{ width: '50%', display: 'flex', justifyContent: 'space-around' }} component="div">
                        <Typography sx={{ display: 'flex', flexDirection: 'column', margin: '0.2rem' }} component="div">
                            <Typography className={styles.tabletitletwo} variant="h5" component="div">
                                RECORD:
                            </Typography>
                            <Typography className={styles.tabletitletwo} variant="h5" component="div">
                                ATS:
                            </Typography>
                            <Typography className={styles.tabletitletwo} variant="h5" component="div">
                                L10:
                            </Typography>
                        </Typography>
                        <Typography sx={{ display: 'flex', flexDirection: 'column' }} component="div">
                            <Typography className={styles.tablesubtitletwo} variant="h5" component="div">
                                {data.awayWins} - {data.awayLosses}
                            </Typography>
                            <Typography className={styles.tablesubtitletwo} variant="h5" component="div">
                                {data.awayConferenceLosses} - {data.awayConferenceLosses} - 0
                            </Typography>
                            <Typography className={styles.tablesubtitletwo} variant="h5" component="div">
                                {data.awayTeamGameTrendsWins} - {data.awayTeamGameTrendsLosses}
                            </Typography>

                        </Typography>

                        <Typography sx={{ display: 'flex', flexDirection: 'column' }} component="div">
                            <Typography className={styles.tabletitletwo} variant="h5" component="div">
                                (0 - 0 Road)
                            </Typography>
                            <Typography className={styles.tabletitletwo} variant="h5" component="div">
                                (0 - 0 - 0 Road)
                            </Typography>
                            <Typography className={styles.tabletitletwo} variant="h5" component="div">
                                (0 - 0 - 0 ATS)
                            </Typography>
                        </Typography>
                    </Typography>


                    <Typography className={styles.subtablatwo} sx={{ width: '50%', display: 'flex', justifyContent: 'space-around' }} component="div">
                        <Typography sx={{ display: 'flex', flexDirection: 'column', margin: '0.2rem' }} component="div">
                            <Typography className={styles.tabletitletwo} variant="h5" component="div">
                                RECORD:
                            </Typography>
                            <Typography className={styles.tabletitletwo} variant="h5" component="div">
                                ATS:
                            </Typography>
                            <Typography className={styles.tabletitletwo} variant="h5" component="div">
                                L10:
                            </Typography>
                        </Typography>
                        <Typography sx={{ display: 'flex', flexDirection: 'column' }} component="div">
                            <Typography className={styles.tablesubtitletwo} variant="h5" component="div">
                                {data.homeTeamGameTrendsWins} - {data.homeTeamGameTrendsLosses}
                            </Typography>
                            <Typography className={styles.tablesubtitletwo} variant="h5" component="div">
                                {data.homeConferenceWins} - {data.homeConferenceLosses} - 0
                            </Typography>
                            <Typography className={styles.tablesubtitletwo} variant="h5" component="div">
                                {data.homeTeamGameTrendsWins} - {data.homeTeamGameTrendsLosses}
                            </Typography>

                        </Typography>

                        <Typography sx={{ display: 'flex', flexDirection: 'column' }} component="div">
                            <Typography className={styles.tabletitletwo} variant="h5" component="div">
                                (7 - 1 Home)
                            </Typography>
                            <Typography className={styles.tabletitletwo} variant="h5" component="div">
                                (4 - 1 - 0 Home)
                            </Typography>
                            <Typography className={styles.tabletitletwo} variant="h5" component="div">
                                (6 - 2 - 0 ATS)
                            </Typography>
                        </Typography>
                    </Typography>



                </Typography>


                <Typography className={styles.cajaNegratwo} component="div">
                    <Typography sx={{ display: 'flex', alignItems: 'center' }} component="div">
                        <Checkbox className={styles.checked} checked={true} color="default" size="small" />
                        <Typography className={styles.title} sx={{ fontSize: '14px', marginTop: '1rem' }} gutterBottom variant="h1" component="div">
                            DePaul at St.Jonn's
                        </Typography>
                    </Typography>

                    <Typography className={styles.title} sx={{ fontSize: '14px', marginRight: '0.8rem' }} gutterBottom variant="h1" component="div">
                        Big East
                    </Typography>
                </Typography>
            </CardActionArea>
        </Card>
    );
}
export default ActionAreaCard;