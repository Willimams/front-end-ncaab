import * as React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { CardActionArea } from "@mui/material";
import styles from "./demo.module.css";

import { pink } from '@mui/material/colors';
import Checkbox from '@mui/material/Checkbox';

const label = { inputProps: { 'aria-label': 'Checkbox demo' } };

/*const CountriesTable = ({ countries }) => {*/
const ActionAreaCard = (data) => {
  //  console.log(data)
  
    return (
        <Card sx={{ maxWidth: '100%' }}>

            <CardActionArea>

                <Typography className={styles.cajaNegratwo} component="div">
                    <Typography sx={{ display: 'flex', alignItems: 'center' }} component="div">
                        <Checkbox sx={{ color: '#9C9C9C', top: '0.8rem' }} checked={true} color="default" size="small" />
                        <Typography className={styles.title} sx={{ fontSize: '14px', marginTop: '2rem' }} gutterBottom variant="h1" component="div">
                            {data.awayTeam} at {data.homeTeam}
                        </Typography>
                    </Typography>

                    <Typography className={styles.title} sx={{ fontSize: '14px', marginRight: '0.8rem' }} gutterBottom variant="h1" component="div">
                        {data.conference}
                    </Typography>
                </Typography>

                <CardContent sx={{ padding: '0', margin: '0', display: 'flex', justifyContent: 'space-between', alignItems: 'center' }} >

                    {/*Container Left*/}
                    <CardContent className={styles.containerleft} sx={{
                        display: 'flex', flexDirection: 'column', alignItems: 'flex-start'
                    }} >
                        <Typography className={styles.subboxleft}
                            component="div">

                            <Typography sx={{ marginRight: '0.4rem', fontWeight: '800' }} gutterBottom variant="h5" component="div">
                                {data.awayTeam}
                            </Typography>
                            <Typography sx={{ fontSize: '14px', color: ' rgb(97, 97, 97)' }}
                                gutterBottom variant="h5" component="div">
                                {data.awayTeamID}
                            </Typography>
                        </Typography>

                        <CardMedia
                            component="img"
                            /*height="140"*/

                            image={data.awayTeamLogoUrl}
                            alt="image"

                            className={styles.img}
                        />
                        <Typography  className={styles.textInf} gutterBottom variant="h5" component="div">
                            O/U MARGIN:
                        </Typography>
                        <Typography sx={{ fontWeight: '800' }} gutterBottom variant="h5" component="div">
                            U: 4

                        </Typography>
                    </CardContent>

                    {/*Contianer middle */}
                    <CardContent sx={{

                        margin: '0', padding: '0', display: 'flex', flexDirection: 'column', alignItems: 'center'
                    }}>
                        <CardContent sx={{ display: 'flex', justifyContent: 'center', padding: '0' }}>
                            <Typography className={styles.scoreleft} gutterBottom variant="h5" component="div">
                                {data.awayTeamScore}
                            </Typography>
                            <Typography className={styles.iconsimulation} sx={{
                                background: '#093D27', borderRadius: '30px',
                                padding: '0.3rem', color: 'white', fontSize: '12px', height: '15px',
                                marginTop: '0.5rem', marginLeft: '1rem', marginRight: '1rem'
                            }}
                                gutterBottom variant="h5" component="div">
                                AT
                            </Typography>
                            <Typography className={styles.scoreright} gutterBottom variant="h5" component="div">
                                {data.homeTeamScore}
                            </Typography>

                        </CardContent>
                        <Typography sx={{ fontWeight: '600', color: '#075A37' }} gutterBottom variant="h5" component="div">
                            {data.period}inal
                        </Typography>
                        <Typography sx={{ display: 'flex', justifyContent: 'center' }} component="div">
                            <CardContent className={styles.box}>
                                <Typography sx={{ color: 'rgb(187, 187, 187)' }} className={styles.tablatitle} gutterBottom variant="h5" component="div">
                                    Lizard
                                </Typography>
                                <Typography sx={{}} className={styles.tablatitle1} gutterBottom variant="h5" component="div">
                                    {data.awayTeam}
                                </Typography>
                                <Typography sx={{}} className={styles.tablatitle1} gutterBottom variant="h5" component="div">
                                    {data.homeTeam}
                                </Typography>
                            </CardContent>

                            <CardContent className={styles.box} >
                                <Typography className={styles.tablatitle} gutterBottom variant="h5" component="div">
                                    1
                                </Typography>
                                <Typography className={styles.tablatitle1} gutterBottom variant="h5" component="div">
                                    {data.periodsAwayScore}
                                </Typography>
                                <Typography className={styles.tablatitle1} gutterBottom variant="h5" component="div">
                                    {data.periodsHomeScore}
                                </Typography>
                            </CardContent>

                            <CardContent className={styles.box} >
                                <Typography className={styles.tablatitle} gutterBottom variant="h5" component="div">
                                    2
                                </Typography>
                                <Typography className={styles.tablatitle1} gutterBottom variant="h5" component="div">
                                    {data.periodsAwayScoreX}
                                </Typography>
                                <Typography className={styles.tablatitle1} gutterBottom variant="h5" component="div">
                                    {data.periodsHomeScoreX}
                                </Typography>
                            </CardContent>

                            <CardContent className={styles.box} >
                                <Typography className={styles.tablatitle} gutterBottom variant="h5" component="div">
                                    T
                                </Typography>
                                <Typography className={styles.tablatitle1} gutterBottom variant="h5" component="div">
                                    {data.awayTeamScore}
                                </Typography>
                                <Typography className={styles.tablatitle1} gutterBottom variant="h5" component="div">
                                    {data.homeTeamScore}
                                </Typography>
                            </CardContent>

                            <CardContent className={styles.box} >
                                <Typography className={styles.tablatitle} gutterBottom variant="h5" component="div">
                                    ODDS
                                </Typography>
                                <Typography className={styles.tablatitle1} gutterBottom variant="h5" component="div">

                                    {data.overUnder}
                                </Typography>
                                <Typography className={styles.tablatitle1} gutterBottom variant="h5" component="div">
                                    {data.pointSpread}
                                </Typography>
                            </CardContent>

                            <CardContent className={styles.box}>
                                <Typography sx={{ color: 'rgb(187, 187, 187)' }} className={styles.tablatitle} gutterBottom variant="h5" component="div">
                                    Lizard
                                </Typography>
                                <Typography sx={{}} className={styles.tablatitle1} gutterBottom variant="h5" component="div">
                                    X
                                </Typography>
                                <Typography sx={{}} className={styles.tablatitle1} gutterBottom variant="h5" component="div">

                                </Typography>
                            </CardContent>

                        </Typography>
                    </CardContent>

                    {/*Container Right*/}
                    <CardContent className={styles.containerleft} sx={{
                        display: 'flex', flexDirection: 'column', alignItems: 'flex-end'
                    }}>
                        <Typography className={styles.subboxright}

                            component="div">
                            <Typography sx={{ marginRight: '0.4rem', fontSize: '14px' }}
                                gutterBottom variant="h5" component="div">
                                {data.homeTeamID}
                            </Typography>
                            <Typography sx={{ fontWeight: '800' }} gutterBottom variant="h5" component="div">
                                {data.homeTeam}
                            </Typography>

                        </Typography>

                        <CardMedia
                            component="img"
                            image={data.homeTeamLogoUrl}
                            alt="image"
                            className={styles.img}
                        />
                        <Typography className={styles.textInf} gutterBottom variant="h5" component="div">
                            COVER BY:
                        </Typography>
                        <Typography sx={{ fontWeight: '800' }} gutterBottom variant="h5" component="div">
                            + {data.liveOddsAwayPointSpread}
                        </Typography>
                    </CardContent>


                </CardContent>
                <Typography className={styles.subtitulos} gutterBottom variant="h5" component="div">
                    Boxscore  Recap Consensus Line Movement
                </Typography>

                <Typography className={styles.subtitulosInf} gutterBottom variant="h5" component="div">
                    BET GRAPH
                </Typography>

                <Typography className={styles.cajaNegra} component="div">
                    <Checkbox className={styles.checked} checked={true} color="default" size="small" />
                    <Typography className={styles.title} sx={{ fontSize: '14px' }} gutterBottom variant="h1" component="div">
                        Gouncher at Morgan St.
                    </Typography>
                </Typography>
            </CardActionArea>
        </Card>
    );
}
export default ActionAreaCard;