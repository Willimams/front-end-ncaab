import * as React from 'react';
import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import CircleRoundedIcon from '@mui/icons-material/CircleRounded';
import Router from 'next/router'



export default function BasicTabs() {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  function handleClick() {
    Router.push("/ncaab"); 
    //direccionar
  }

  return (
    
    <Box sx={{ width: '100%' }}>
      <Box sx={{ borderBottom: 0, borderColor: 'divider', marginBottom:2, backgroundColor:'#dad1ce' }}>
         
        <Tabs value={value} onChange={handleChange} TabIndicatorProps={{ sx: { display: 'none' } }}
          value={value}
          onChange={handleChange}
          aria-label="basic tabs example" 
          variant="scrollable"
          scrollButtons="auto"          
          >

          <Card sx={{ minWidth: 275,  margin:1 }} >
            <CardContent >
              <Grid container spacing={0}  >
                <Grid item xs={8} >
                  Dec 18th
                </Grid>
                <Grid item xs={2} >
                  Final                  
                </Grid>

                <Grid item xs={8} >
                  <CircleRoundedIcon fontSize="small" color="success" /> Team Name             
                </Grid>
                <Grid item xs={2} >
                  18                  
                </Grid>

                <Grid item xs={8} >
                  <CircleRoundedIcon fontSize="small" color="success" /> Team Name             
                </Grid>
                <Grid item xs={2} >
                  18                  
                </Grid>
              </Grid>              
            </CardContent>
            <CardActions>
              <Button size="small" onClick={handleClick}>view</Button>
            </CardActions>
          </Card>                   


          <Card sx={{ minWidth: 275,  margin:1 }} >
            <CardContent >
              <Grid container spacing={0}  >
                <Grid item xs={8} >
                  Dec 18th
                </Grid>
                <Grid item xs={2} >
                  Final                  
                </Grid>

                <Grid item xs={8} >
                  <CircleRoundedIcon fontSize="small" color="success" /> Team Name             
                </Grid>
                <Grid item xs={2} >
                  18                  
                </Grid>

                <Grid item xs={8} >
                  <CircleRoundedIcon fontSize="small" color="success" /> Team Name             
                </Grid>
                <Grid item xs={2} >
                  18                  
                </Grid>
              </Grid>              
            </CardContent>
            <CardActions>
              <Button size="small">view</Button>
            </CardActions>
          </Card>   

          <Card sx={{ minWidth: 275,  margin:1 }} >
            <CardContent >
              <Grid container spacing={0}  >
                <Grid item xs={8} >
                  Dec 18th
                </Grid>
                <Grid item xs={2} >
                  Final                  
                </Grid>

                <Grid item xs={8} >
                  <CircleRoundedIcon fontSize="small" color="success" /> Team Name             
                </Grid>
                <Grid item xs={2} >
                  18                  
                </Grid>

                <Grid item xs={8} >
                  <CircleRoundedIcon fontSize="small" color="success" /> Team Name             
                </Grid>
                <Grid item xs={2} >
                  18                  
                </Grid>
              </Grid>              
            </CardContent>
            <CardActions>
              <Button size="small">view</Button>
            </CardActions>
          </Card>   


          <Card sx={{ minWidth: 275,  margin:1 }} >
            <CardContent >
              <Grid container spacing={0}  >
                <Grid item xs={8} >
                  Dec 18th
                </Grid>
                <Grid item xs={2} >
                  Final                  
                </Grid>

                <Grid item xs={8} >
                  <CircleRoundedIcon fontSize="small" color="success" /> Team Name             
                </Grid>
                <Grid item xs={2} >
                  18                  
                </Grid>

                <Grid item xs={8} >
                  <CircleRoundedIcon fontSize="small" color="success" /> Team Name             
                </Grid>
                <Grid item xs={2} >
                  18                  
                </Grid>
              </Grid>              
            </CardContent>
            <CardActions>
              <Button size="small">view</Button>
            </CardActions>
          </Card>   

                    
                    

          <Card sx={{ minWidth: 275,  margin:1 }} >
            <CardContent >
              <Grid container spacing={0}  >
                <Grid item xs={8} >
                  Dec 18th
                </Grid>
                <Grid item xs={2} >
                  Final                  
                </Grid>

                <Grid item xs={8} >
                  <CircleRoundedIcon fontSize="small" color="success" /> Team Name             
                </Grid>
                <Grid item xs={2} >
                  18                  
                </Grid>

                <Grid item xs={8} >
                  <CircleRoundedIcon fontSize="small" color="success" /> Team Name             
                </Grid>
                <Grid item xs={2} >
                  18                  
                </Grid>
              </Grid>              
            </CardContent>
            <CardActions>
              <Button size="small">view</Button>
            </CardActions>
          </Card>   


          <Card sx={{ minWidth: 275,  margin:1 }} >
            <CardContent >
              <Grid container spacing={0}  >
                <Grid item xs={8} >
                  Dec 18th
                </Grid>
                <Grid item xs={2} >
                  Final                  
                </Grid>

                <Grid item xs={8} >
                  <CircleRoundedIcon fontSize="small" color="success" /> Team Name             
                </Grid>
                <Grid item xs={2} >
                  18                  
                </Grid>

                <Grid item xs={8} >
                  <CircleRoundedIcon fontSize="small" color="success" /> Team Name             
                </Grid>
                <Grid item xs={2} >
                  18                  
                </Grid>
              </Grid>              
            </CardContent>
            <CardActions>
              <Button size="small">view</Button>
            </CardActions>
          </Card>   

                    
                    

                    
                    
                    
                    


        </Tabs>
      </Box>
      
    </Box>
  );
}