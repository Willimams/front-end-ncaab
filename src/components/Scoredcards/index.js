import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';

export default function Scorecards() {
  return (
    <Card >
      <CardActionArea>
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            Scorecards
          </Typography>
          <Typography variant="body2" color="text.secondary">
            New Section
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}