import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'


import CardsView from '../components/view/cardsView'

import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";

import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';

import Layout from '../components/layout'
import { Card } from '@mui/material';



export default function Home() {
  return (
    <div>


      <Layout>
        {/*<Grid container spacing={4} >
          <Grid item md={8}>
            Get the latest NCAA college basketball news, the official March Madness bracket, highlights and scores from every division in men's college hoops.
          </Grid>
          <Grid item md={4}>
            <Grid item md={2}>
              SCORES
            </Grid>
            <Grid item md={2}>
              WIDGETS
            </Grid>
          </Grid>
        </Grid>*/}

        <CardsView />
      </Layout>





    </div>

  )
}
